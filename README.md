# voiceassistant

simple voice assistant for android OS

this is a basic form of voice assistant for android like Google Assistant. 

i named her BAHAREH (pronounced: Bah Haa Reh) , after someone special. i dont know how your phone pronounces it though :))

i used embedded android and google library like TTS for speech and text recognition.

i fixed the extra permission required for recording audio needed in Android 6 and higher. you still need to add permission to Manifest file.

as you know the basic idea of voice assistants is just If and Else If blocks so you can easily expand on my code and create new classes for more features.

i will soon expand it myself too. this is just a prototype.

for the XML file you will only need a Floating Action Button. ;)